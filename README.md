This is a demo project to reproduce 'onClick' bug.

# UPDATE
It seems the issue is already reported
https://github.com/babel/babel/issues/4377

# Running
1. execute `npm run webpack`
2. open `client/index.html`

# Behavior
Page is empty and console has 'warning' about inappropriate component type.

# Expected
Link 'Foobar' which 'alert'-s on click.

# Note
If 'onClick' property is renamed to e.g. onBarClick then bug disappears.
